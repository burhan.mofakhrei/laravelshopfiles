const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

// elixir((mix) = > {
//     mix.sass('app.scss')
//     .webpack('app.js');
// });
elixir(function (mix) {
    mix.styles([
        './resources/assets/css/bootstrap-rtl.min.css',
        './resources/assets/css/select2.min.css',
        './resources/assets/css/admin-custom.css',
    ], 'public/css/app.css');
    mix.scripts([
        './resources/assets/js/select2.min.js',
        './resources/assets/js/custom-admin.js',
    ], 'public/js/app.js');


    mix.styles([
        './resources/assets/css/bootstrap-rtl.min.css',
        './resources/assets/css/frontend-custom.css',
    ], 'public/css/frontend.css');
    mix.scripts([
        './resources/assets/js/custom-frontend.js',
    ], 'public/js/frontend.js');
});


