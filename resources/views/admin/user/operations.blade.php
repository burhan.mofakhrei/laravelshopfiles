<a href="{{ route('admin.users.edit',$user->id)  }}"><i class="glyphicon glyphicon-edit"></i></a>
<a href="{{ route('admin.users.delete',$user->id)  }}"><i class="glyphicon glyphicon-trash"></i></a>
<a title="نمایش لیست پکیج های خریداری شده" href="{{ route('admin.users.packages',$user->id)  }}"><i class="glyphicon glyphicon-list"></i></a>