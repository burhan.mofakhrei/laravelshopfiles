<tr>
    <td>{{ $user->id  }}</td>
    <td>{{ $user->name  }}</td>
    <td>{{ $user->email  }}</td>
    <td>{{ $user->wallet  }}</td>
    <td>{{ $user->packages()->count()  }}</td>
    <td style="text-align: center;">
        @include('admin.user.operations',$user)
    </td>
</tr>