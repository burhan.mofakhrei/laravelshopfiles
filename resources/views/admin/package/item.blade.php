<tr>
    <td>{{  $package->package_title  }}</td>
    <td>{{  $package->package_price  }}</td>
    <td>{{ $package->files()->get()->count()  }}</td>
    <td>

        <a href="{{ route('admin.packages.edit',[$package->package_id])  }}">Edit</a>
        <a href="{{ route('admin.packages.remove',[$package->package_id])  }}">Remove</a>
        <a href="{{ route('admin.packages.sync_files',[$package->package_id])  }}">Files</a>
    </td>

</tr>