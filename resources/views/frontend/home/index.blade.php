@extends('layouts.frontend')

@section('content')
    <div class="col-xs-9 col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">آخرین فایل های سیستم</div>
            <div class="panel-body">
                @if($files && count($files) > 0)
                    <ul>
                    @foreach($files as  $file)
                      <li>  <a href="{{ route('frontend.files.details',$file->file_id)  }}">{{ $file->file_title  }}</a></li>
                    @endforeach
                    </ul>
                @endif
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">آخرین پکیج ها</div>
            <div class="panel-body">
                @if($packages && count($packages) > 0)
                    <ul>
                        @foreach($packages as  $package)
                            <li>  <a href="{{ route('frontend.packages.details',[$package->package_id])  }}">{{ $package->package_title  }}</a></li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
    <div class="col-xs-3 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">لیست دسته بندی های فایل ها</div>
            <div class="panel-body">
                @if($categories && count($categories) > 0)
                    <ul>
                        @foreach($categories as  $category)
                            <li>  <a href="{{ route('frontend.category.item',[$category->category_id])  }}">{{ $category->category_name  }}</a></li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>

    </div>
    <div class="col-xs-3 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">محبوب ترین فایل ها</div>
            <div class="panel-body">
                @if($popularFiles && count($popularFiles) > 0)
                    <ul>
                        @foreach($popularFiles as  $file)
                            <li>  <a href="{{ route('frontend.files.details',[$file->file_id])  }}">{{ $file->file_title  }}</a></li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        @if(\Illuminate\Support\Facades\Auth::check())
        <div class="panel panel-default">
            <div class="panel-heading">آخرین فعالیت</div>
            <div class="panel-body">
                {{ session('last_activity')  }}
            </div>
        </div>
            @endif
    </div>
@endsection