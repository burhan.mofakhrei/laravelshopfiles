@extends('layouts.frontend')

@section('content')
    <div class="col-xs-9 col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">مشاهده جزئیات پکیج</div>
            <div class="panel-body">
                <p>عنوان : {{ $packItem->package_title }}</p>
                <p>لیست فایل های این پکیج :</p>
                <ul>
                @foreach($packgeFiles as $file)
                    <li><a href="{{ route('frontend.files.details',[$file->file_id]) }}">{{ $file->file_title  }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-3 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">خرید و دانلود پکیج</div>
            <div class="panel-body">
            </div>
        </div>
    </div>
    @endsection