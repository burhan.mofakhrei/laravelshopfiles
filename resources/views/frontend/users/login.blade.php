@extends('layouts.frontend')
@section('content')
    <div class="col-xs-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">ورود به سایت</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        @if(session('loginError'))
                            <div class="alert alert-danger">
                                <p>
                                    {{ session('loginError')  }}
                                </p>
                            </div>
                        @endif
                        <form class="form-horizontal" method="post" action="{{  route('post.login')  }}">
                            {{ csrf_field()  }}
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">ایمیل :</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email" id="inputEmail3">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">کلمه عبور :</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password" id="inputPassword3">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox"> مرا به خاطر بسپار
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">ورود</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop