@extends('layouts.frontend')

@section('content')
    <div class="col-xs-9 col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">لیست تمام فایل های دسته بندی :{{ $categoryItem->category_name }} </div>
            <div class="panel-body">
                <ul>
                    @foreach($categoryFiles as $file)
                        <li><a href="{{ route('frontend.files.details',[$file->file_id]) }}">{{ $file->file_title  }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-9 col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">لیست تمام پکیج های دسته بندی :{{ $categoryItem->category_name }} </div>
            <div class="panel-body">
                <ul>
                    @foreach($categoryPackges as $package)
                        <li><a href="{{ route('frontend.packages.details',[$package->package_id]) }}">{{ $package->package_title  }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection