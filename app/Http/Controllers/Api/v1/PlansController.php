<?php
/**
 * Created by PhpStorm.
 * User: Alimohammadi
 * Date: 4/5/2018
 * Time: 19:02
 */

namespace App\Http\Controllers\Api\v1;


use App\Models\Plan;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class PlansController {
	public function index() {
		$fractal = new Manager();

		$plans =  Plan::get();
//		$newData = [];
//		foreach ($plans as $plan)
//		{
//			$newData[]=[
//				'id' => $plan->plan_id,
//				'name' => $plan->plan_title,
//				'amount' => number_format($plan->plan_price)
//			];
//		}
		$resources =  new Collection($plans,function ($plan){

			return [
				'id' => $plan->plan_id,
				'name' => $plan->plan_title,
				'amount' => $plan->plan_price
			];

		});

		return response()->json($fractal->createData($resources)->toArray());
	}
}