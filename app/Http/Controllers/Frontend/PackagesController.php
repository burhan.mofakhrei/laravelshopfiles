<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
	public function details(Request $request,$pack_id) {
		$packItem = Package::find($pack_id);
		$packgeFiles = $packItem->files;
		return view('frontend.packages.details',compact('packItem','packgeFiles'));
    }
}
