<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\UserSubscribed;
use App\Mellat;
use App\Models\Plan;
use App\Models\Subscribe;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SubscribeController extends Controller {

	public function index( Request $request, int $plan_id ) {
		$plan = Plan::find( $plan_id );

		return view( 'frontend.subscribe.index', compact( 'plan' ) );
	}

	public function register( Request $request, $plan_id ) {

	}

}
