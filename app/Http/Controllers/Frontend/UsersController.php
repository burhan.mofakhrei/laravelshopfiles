<?php

namespace App\Http\Controllers\Frontend;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailNotify;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function login()
    {
        return view('frontend.users.login');
    }

    public function doLogin(Request $request)
    {
//        $this->validate($request,[],[]);
        $remember = $request->has('remember');
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $remember)) {
            $user = Auth::user();
            $job = (new SendEmailNotify($user))->onQueue('emails');
            $job->delay(Carbon::now()->addMinutes(15));
            dispatch($job);
            return redirect('/');
        }

        return redirect()->back()->with('loginError', 'ایمیل یا کلمه عبور اشتباه می باشد');

    }

    public function register()
    {
        return view('frontend.users.register');
    }

    public function doRegister(Request $request)
    {
//        $this->validate($request,[],[]);
        $newUserDate = [
            'name' => $request->input('fullName'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        $newUser = User::create($newUserDate);
        if ($newUser && $newUser instanceof User) {
            event(new UserRegistered($newUser));
            return redirect('/');
        }
        return redirect()->back()->with('registerError', 'در فرآیند ثبت نام خطایی رخ داده است. لطفا بعدا امتحان کنید');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
