<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Facades\jDate;

class CategoriesController extends Controller {
	public function index() {

	}

	public function item( Request $request, $category_id ) {
		$categoryItem = Category::find($category_id);
		$categoryFiles = $categoryItem->files;
		$categoryPackges = $categoryItem->packages;
		return view('frontend.category.item',compact('categoryItem','categoryFiles','categoryPackges'));
	}
}
