<?php

namespace App\Http\Controllers\Frontend;

use App\Models\File;
use App\Utility\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FilesController extends Controller {
	public function details( Request $request, int $file_id ) {
		$file_item    = File::find( $file_id );
		$current_user = Auth::user()->id;
		return view( 'frontend.files.single', compact( 'file_item', 'current_user' ) );
	}

	public function download( Request $request, int $file_id ) {
		$current_user = Auth::user();
		if ( ! \App\Utility\File::user_can_download( $current_user->id ) ) {
			return redirect()->route( 'frontend.files.access' );
		}
		$fileItem = File::find( $file_id );
		if ( ! $fileItem ) {
			return redirect()->back()->with( 'fileError', 'فایل درخواستی معتبر نمی باشد' );
		}
		$basePath = public_path( 'images\\' );
		$filePath = $basePath . $fileItem->file_name;
		$currentUserSubscribe= $current_user->currentSubscribe()->first();
		$currentUserSubscribe->increment('subscribe_download_count');
		$fileItem->increment('file_download_count');
		$fileItem->updateDownloadCounts();
		return response()->download( $filePath );
	}

	public function access() {
		return view( 'frontend.files.access' );
	}

	public function report( Request $request ) {
		$file_id = $request->input( 'file_id' );
		if ( $file_id && intval( $file_id ) > 0 ) {
			$fileItem = File::find( $file_id );
			$fileItem->increment( 'file_report_count' );

			return [
				'success' => TRUE,
				'message' => 'درخواست شما با موفقیت ثبت گردید'
			];
		}

		return [
			'success' => FALSE,
			'message' => 'درخواست شما معتبر نمی باشد'
		];
	}
}
