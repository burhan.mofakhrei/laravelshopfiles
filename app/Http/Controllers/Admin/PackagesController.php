<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\File;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    public function index()
    {
        $packages = Package::all();
        return view('admin.package.list',compact('packages'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.package.create',compact('categories'));
    }

    public function store(Request $request)
    {
        //validation
        $new_package = Package::create([
            'package_title' => $request->input('package_title'),
            'package_price' => $request->input('package_price')
        ]);
        if($new_package){
            if($request->has('categorize')) {
                $new_package->categories()->sync($request->input('categorize'));
            }
           return  redirect()->route('admin.packages.list')->with('success','پکیج جدید با موفقیت ساخته شد');
        }
    }

    public function edit(Request $request,$package_id)
    {
        $package_item = Package::find($package_id);
        $categories = Category::all();
        $package_categories = $package_item->categories()->get()->pluck('category_id')->toArray();
        return view('admin.package.edit',compact('package_item','categories','package_categories'));
    }

    public function update(Request $request,$package_id)
    {
        $package_item = Package::find($package_id);
        if($package_item){
            $package_item->update([
                'package_title' => $request->input('package_title'),
                'package_price' => $request->input('package_price')
            ]);
            if($request->has('categorize')){
                $package_item->categories()->sync($request->input('categorize'));
            }
            return  redirect()->route('admin.packages.list')->with('success','پکیج جدید با موفقیت ساخته شد');
        }
    }

    public function remove()
    {

    }

    public function syncFiles(Request $request,$package_id)
    {
        $files = File::all();
        $package_item = Package::find($package_id);
        $package_files = $package_item->files()->get()->pluck('file_id')->toArray();
//        $files_ids = [];
//        foreach ($package_files as $file){
//            $files_ids [] = $file->file_id;
//        }
//        dd($files_ids);
        return view('admin.package.files',compact('files','package_files'))->with(['panel_title' => 'انتخاب فایل های پکیج']);
    }

    public function updateSyncFiles(Request $request,$package_id)
    {
        $package_item = Package::find($package_id);
        $files = $request->input('files');
        if($package_item && is_array($files)){
//            $package_item->files()->attach($files);
            $package_item->files()->sync($files);
            return redirect()->route('admin.packages.list')->with('success','اطلاعات با موفقیت ذخیره شد.');
        }
    }
}
