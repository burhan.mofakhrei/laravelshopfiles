<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    // admin methods
    public function index()
    {
//        $user = User::find(5);
//        $user->packages()->sync([1 => ['amount' => 12000,'created_at' => date('Y-m-d H:i:s')]]);
        $users = User::all();
        return view('admin.user.index', compact('users'))->with(['panel_title' => 'لیست کاربران']);
    }

    public function create()
    {
        return view('admin.user.create')->with(['panel_title' => 'ایجاد کاربر جدید']); // views/admin/users/create.blade.php
    }

    public function store(UserRequest $userRequest)
    {
        $user_data = [
            'name' => request()->input('full_name'),
            'email' => request()->input('email'),
            'password' => request()->input('password'),
            'role' => request()->input('role'),
            'wallet' => request()->input('wallet'),
        ];
        $new_user_object = User::create($user_data);
        if ($new_user_object instanceof User) {
            return redirect()->route('admin.users.list')->with('success', 'کاربر جدید با موفقیت ثبت گردید.');
        }
    }

    public function delete($user_id)
    {
        if ($user_id && ctype_digit($user_id)) {
            $userItem = User::find($user_id);
            if ($userItem && $userItem instanceof User) {
                $userItem->delete();
                return redirect()->route('admin.users.list')->with('success', 'کاربر مورد نظر با موفقیت حذف گردید.');
            }
        }
    }

    public function edit($user_id)
    {
        if ($user_id && ctype_digit($user_id)) {
            $userItem = User::find($user_id);
            if ($userItem && $userItem instanceof User) {
                return view('admin.user.edit', compact('userItem'))->with(['panel_title' => 'ویرایش کاربر']);
            }
        }
    }

    public function update(UserRequest $userRequest, $user_id)
    {
        $inputs = [
            'name' => request()->input('full_name'),
            'email' => request()->input('email'),
            'password' => request()->input('password'),
            'role' => request()->input('role'),
            'wallet' => request()->input('wallet'),

        ];
        if (!request()->has('password')) {
            unset($inputs['password']);
        }

        $userItem = User::find($user_id);
        $userItem->update($inputs);
        return redirect()->route('admin.users.list')->with('success', 'اطلاعات کاربر مورد نظر با موفقیت به روز رسانی شد.');

    }

    public function packages(Request $request,$user_id)
    {
        $user =  User::find($user_id);
        if(!$user){
            return redirect()->back();
        }
        $user_packages = $user->packages()->get();
        return view('admin.user.packages',compact('user_packages'))->with('panel_title','نمایش لیست پکیج های کاربر');
    }
}
