<?php

namespace App\Http\Controllers\Admin;

use App\Models\FileDownload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
	public function index() {

		$results = DB::table('file_downloads')
					->select(DB::raw('sum(download_count) as total_download,DATE(download_date) as date'))
					->groupBy(DB::raw('DATE(download_date)'))->get();
		$download_statistics = [];
		foreach ($results as $item)
		{
			$download_statistics[$item->date] = $item->total_download;
		}
		$panel_title = 'داشبورد مدیریت';
		return view('admin.dashboard.index',compact('panel_title','download_statistics'));
    }
}
