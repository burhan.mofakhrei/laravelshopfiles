<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileDownload extends Model
{
	protected $guarded = ['id'];
	protected $dates = [
		'download_date'
	];
	public $timestamps = false;
}
