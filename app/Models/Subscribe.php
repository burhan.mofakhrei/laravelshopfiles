<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
	protected $primaryKey = 'subscribe_id';
    protected $guarded = ['subscribe_id'];

    public $timestamps = false;

    protected $dates = [
        'subscribe_created_at',
        'subscribe_expired_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'subscribe_user_id');
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class,'subscribe_plan_id');
    }
}
