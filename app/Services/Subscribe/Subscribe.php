<?php
/**
 * Created by PhpStorm.
 * User: Alimohammadi
 * Date: 4/5/2018
 * Time: 16:40
 */

namespace App\Services\Subscribe;


use App\Mail\UserSubscribed;
use App\Models\Plan;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Subscribe {

	public function subscribeUser( int $user_id, int $plan_id ) {
		$plan = Plan::find( $plan_id );
		$user = User::find( $user_id );
		if ( ! $plan ) {
			throw new \Exception( 'invalid plan item.' );
		}
		$plan_days_count = $plan->plan_days_count;
		$expired_at      = Carbon::now();
		$expired_at->addDay( $plan_days_count );
		$subscribeDate = [
			'subscribe_user_id'        => $user_id,
			'subscribe_plan_id'        => $plan_id,
			'subscribe_download_limit' => $plan->plan_limit_download_count,
			'subscribe_created_at'     => Carbon::now(),
			'subscribe_expired_at'     => $expired_at->format( 'Y-m-d H:i:s' )
		];
		$subscribe     = Subscribe::create( $subscribeDate );
		Mail::to( $user )->later( Carbon::now()->addMinutes( 15 ), new UserSubscribed( $subscribe ) );
	}

	public function unsubscribeUser( int $user_id, int $plan_id ) {

	}

}