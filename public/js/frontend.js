jQuery(document).ready(function ($) {
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.btn_report_file', function (event) {

        event.preventDefault();
        var $this = $(this);
        var file_id = $this.data('fid');
        $.ajax({
            url: '/file/report',
            type: 'post',
            dataType: 'json',
            data: {
                file_id: file_id
            },
            success: function (response) {
                if(response.success)
                {
                    swal({
                        text:response.message,
                        title:'',
                        icon:'success'
                    });
                }else{
                    swal({
                        text:response.message,
                        title:'',
                        icon:'error'
                    });
                }
            },
            erorr: function () {

            }
        });

    });
});
