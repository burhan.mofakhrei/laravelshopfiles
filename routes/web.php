<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group( [ 'namespace' => 'Frontend', 'middleware' => 'last_activity' ], function () {

	Route::get( '/', 'HomeController@index' )->name( 'home' );
	//users auth
	Route::get( 'account/login', 'UsersController@login' )->name( 'login' );
	Route::post( 'account/login', 'UsersController@doLogin' )->name( 'post.login' );
	Route::get( 'account/register', 'UsersController@register' )->name( 'register' );
	Route::post( 'account/register', 'UsersController@doRegister' )->name( 'post.register' );
	Route::get( 'account/logout', 'UsersController@logout' )->name( 'logout' );

	//user dashboard

	Route::get( 'dashboard', 'DashboardController@index' )->name( 'user.dashboard' );

	Route::get( '/plans', 'PlansController@index' )->name( 'frontend.plans.index' );
	Route::get( '/subscribe/{plan_id}', 'SubscribeController@index' )->name( 'frontend.subscribe.index' );
	Route::post( '/subscribe/{plan_id}', 'SubscribeController@register' )->name( 'frontend.subscribe.register' );
	//files
	Route::get( '/file/{file_id}', 'FilesController@details' )->name( 'frontend.files.details' );
	Route::get( '/file/download/{file_id}', 'FilesController@download' )->name( 'frontend.files.download' );
	Route::post( '/file/report', 'FilesController@report' )->name( 'frontend.files.report' );
	Route::get( '/access', 'FilesController@access' )->name( 'frontend.files.access' );

	//packages
	Route::get( '/package/{pack_id}', 'PackagesController@details' )->name( 'frontend.packages.details' );

	//categories
	Route::get( 'categories', 'CategoriesController@index' )->name( 'frontend.categories' );
	Route::get( 'category/{category_id}', 'CategoriesController@item' )->name( 'frontend.category.item' );


	//payments
	Route::post('payment/{plan_id}','PaymentsController@redirect')->name('payment.start');
	Route::post( 'payment/mellat/verify', 'PaymentsController@verify' )->name( 'payment.verify' );

} );

Route::group( [ 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin' ], function () {

	Route::get( '/', 'DashboardController@index' )->name( 'admin.dashboard' );
	// users routes
	Route::get( '/users', 'UsersController@index' )->name( 'admin.users.list' );
	Route::get( '/users/create', 'UsersController@create' )->name( 'admin.users.create' );
	Route::post( '/users/create', 'UsersController@store' )->name( 'admin.users.store' );
	Route::get( '/users/delete/{user_id}', 'UsersController@delete' )->name( 'admin.users.delete' );
	Route::get( '/users/edit/{user_id}', 'UsersController@edit' )->name( 'admin.users.edit' );
	Route::post( '/users/edit/{user_id}', 'UsersController@update' )->name( 'admin.users.update' );
	Route::get( '/users/packages/{user_id}', 'UsersController@packages' )->name( 'admin.users.packages' );

	//files routes
	Route::get( '/files', 'FilesController@index' )->name( 'admin.files.list' );
	Route::get( '/files/create', 'FilesController@create' )->name( 'admin.files.create' );
	Route::post( '/files/create', 'FilesController@store' )->name( 'admin.files.store' );
	Route::get( '/files/edit/{file_id}', 'FilesController@edit' )->name( 'admin.files.edit' );
	Route::post( '/files/edit/{file_id}', 'FilesController@update' )->name( 'admin.files.update' );

	//plans routes
	Route::get( '/plans', 'PlansController@index' )->name( 'admin.plans.list' );
	Route::get( '/plans/create', 'PlansController@create' )->name( 'admin.plans.create' );
	Route::post( '/plans/create', 'PlansController@store' )->name( 'admin.plans.store' );
	Route::get( '/plans/edit/{plan_id}', 'PlansController@edit' )->name( 'admin.plans.edit' );
	Route::post( '/plans/edit/{plan_id}', 'PlansController@update' )->name( 'admin.plans.update' );
	Route::get( '/plans/remove/{plan_id}', 'PlansController@remove' )->name( 'admin.plans.remove' );

	//packages routes
	Route::get( '/packages', 'PackagesController@index' )->name( 'admin.packages.list' );
	Route::get( '/packages/create', 'PackagesController@create' )->name( 'admin.packages.create' );
	Route::post( '/packages/create', 'PackagesController@store' )->name( 'admin.packages.store' );
	Route::get( '/packages/edit/{package_id}', 'PackagesController@edit' )->name( 'admin.packages.edit' );
	Route::post( '/packages/edit/{package_id}', 'PackagesController@update' )->name( 'admin.packages.update' );
	Route::get( '/packages/remove/{package_id}', 'PackagesController@remove' )->name( 'admin.packages.remove' );
	Route::get( '/packages/sync_files/{package_id}', 'PackagesController@syncFiles' )->name( 'admin.packages.sync_files' );
	Route::post( '/packages/sync_files/{package_id}', 'PackagesController@updateSyncFiles' )->name( 'admin.packages.sync_files' );

	//Payments routes
	Route::get( '/payments', 'PaymentsController@index' )->name( 'admin.payments.list' );
//    Route::get('/payments/create', 'PaymentsController@create')->name('admin.payments.create');
//    Route::post('/payments/create', 'PaymentsController@store')->name('admin.payments.store');
//    Route::get('/payments/edit/{payment_id}', 'PaymentsController@edit')->name('admin.payments.edit');
//    Route::post('/payments/edit/{payment_id}', 'PaymentsController@update')->name('admin.payments.update');
	Route::get( '/payments/remove/{payment_id}', 'PaymentsController@remove' )->name( 'admin.payments.remove' );

	//packages routes
	Route::get( '/categories', 'CategoriesController@index' )->name( 'admin.categories.list' );
	Route::get( '/categories/create', 'CategoriesController@create' )->name( 'admin.categories.create' );
	Route::post( '/categories/create', 'CategoriesController@store' )->name( 'admin.categories.store' );
	Route::get( '/categories/edit/{category_id}', 'CategoriesController@edit' )->name( 'admin.categories.edit' );
	Route::post( '/categories/edit/{category_id}', 'CategoriesController@update' )->name( 'admin.categories.update' );
	Route::get( '/categories/remove/{category_id}', 'CategoriesController@remove' )->name( 'admin.categories.remove' );
} );


