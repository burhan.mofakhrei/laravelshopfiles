<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login','Api\AuthController@login');

Route::resource( 'users', 'Api\UsersController' );

Route::group(['prefix' => 'v1','namespace' => 'Api\v1','middleware' => 'jwt.auth'],function(){
	Route::get('plans','PlansController@index');
});

Route::group(['prefix' => 'v2','namespace' => 'Api\v2'],function(){
	Route::get('plans','PlansController@index');
});